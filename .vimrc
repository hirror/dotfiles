"---------------------------
" Start Neobundle Settings.
"---------------------------
" bundleで管理するディレクトリを指定
set runtimepath+=~/.vim/bundle/neobundle.vim/
 
" Required:
call neobundle#begin(expand('~/.vim/bundle/'))
 
" neobundle自体をneobundleで管理
NeoBundleFetch 'Shougo/neobundle.vim'
 
" 今後このあたりに追加のプラグインをどんどん書いて行きます！！"

  "ファイルオープンを便利に
  NeoBundle 'Shougo/unite.vim'
  "Unite.vimで最近使ったファイルを表示できるようにする
  NeoBundle 'Shougo/neomru.vim'
  " ...省略
  " ファイルをtree表示してくれる
  NeoBundle 'scrooloose/nerdtree'
  " コメントON/OFFを手軽に実行
  NeoBundle 'tomtom/tcomment_vim'
  " シングルクオートとダブルクオートの入れ替え等
  NeoBundle 'tpope/vim-surround'
  " インデントに色を付けて見やすくする
  NeoBundle 'nathanaelkane/vim-indent-guides'
  " \cで手軽にコメントアウトと解除"
  NeoBundle "tyru/caw.vim.git"

call neobundle#end()
 
" Required:
filetype plugin indent on
 
" 未インストールのプラグインがある場合、インストールするかどうかを尋ねてくれるようにする設定
" 毎回聞かれると邪魔な場合もあるので、この設定は任意です。
NeoBundleCheck
 
"-------------------------
" End Neobundle Settings.
"-------------------------





" """"""""""""""""""""""""""""""
" " Unit.vimの設定
" """"""""""""""""""""""""""""""
" http://blog.remora.cx/2010/12/vim-ref-with-unite.html
" " 入力モードで開始する
 let g:unite_enable_start_insert=1
" " バッファ一覧
 noremap <C-P> :Unite buffer<CR>
" " ファイル一覧
 noremap <C-N> :Unite -buffer-name=file file<CR>
" " 最近使ったファイルの一覧
 noremap <C-Z> :Unite file_mru<CR>
" " sourcesを「今開いているファイルのディレクトリ」とする
 noremap :uff :<C-u>UniteWithBufferDir file -buffer-name=file<CR>
" " ウィンドウを分割して開く
 au FileType unite nnoremap <silent> <buffer> <expr> <C-J> unite#do_action('split')
 au FileType unite inoremap <silent> <buffer> <expr> <C-J> unite#do_action('split')
 " ウィンドウを縦に分割して開く
 au FileType unite nnoremap <silent> <buffer> <expr> <C-K> unite#do_action('vsplit')
 au FileType unite inoremap <silent> <buffer> <expr> <C-K> unite#do_action('vsplit')
" " ESCキーを2回押すと終了する
 au FileType unite nnoremap <silent> <buffer> <ESC><ESC> :q<CR>
 au FileType unite inoremap <silent> <buffer> <ESC><ESC> <ESC>:q<CR>

 
""""""""""""""""""""""""""""""
" vim-indent-guidesの設定
""""""""""""""""""""""""""""""
" vimを立ち上げたときに、自動的にvim-indent-guidesをオンにする
" let g:indent_guides_enable_on_vim_startup = 1


""""""""""""""""""""""""""""""
"挿入モード時、ステータスラインの色を変更
" https://sites.google.com/site/fudist/Home/vim-nihongo-ban/-vimrc-sample
""""""""""""""""""""""""""""""
  let g:hi_insert = 'highlight StatusLine guifg=darkblue guibg=darkyellow gui=none ctermfg=blue ctermbg=yellow cterm=none'
 
 if has('syntax')
   augroup InsertHook
       autocmd!
       autocmd InsertEnter * call s:StatusLine('Enter')
       autocmd InsertLeave * call s:StatusLine('Leave')
   augroup END
 endif
 
 let s:slhlcmd = ''
 function! s:StatusLine(mode)
   if a:mode == 'Enter'
      silent! let s:slhlcmd = 'highlight ' .s:GetHighlight('StatusLine')
      silent exec g:hi_insert
   else
      highlight clear StatusLine
      silent exec s:slhlcmd
   endif
  endfunction

  function! s:GetHighlight(hi)
  redir => hl
  exec 'highlight '.a:hi
  redir END
  let hl = substitute(hl,'[\r\n]', '', 'g')
  let hl = substitute(hl,'xxx', '', '')
  return hl
 endfunction
"""""""""""""""""""""""""""""





"""""""""""""""""""""""""""""
" caw.vim の設定（\cで手軽にコメントアウトと解除）
"""""""""""""""""""""""""""""
nmap <Leader>c <Plug>(caw:i:toggle)
vmap <Leader>c <Plug>(caw:i:toggle)





"####表示設定####
"set number "行番号を表示
set title "編集中のファイル名を表示
syntax on "コードの色分け
set expandtab "タブの代わりにスペースを使用"
set tabstop=2 "インデントをスペース2つ分に設定
set shiftwidth=2 "インデントの幅
set autoindent "改行時に前の行のインデントを継続する
set showmatch "対応する括弧を表示
"
""####検索設定####
set ignorecase "大文字/小文字を区別しない
set smartcase "検索文字列に大文字が含まれている場合は区別する

"####クリップボード####
"set clipboard+=unnamed

"#######ファイルをまたいだヤンクデータの保持#########
set viminfo='20,\"1000

"####補完#####
inoremap {<Enter> {}<Left><CR><ESC><S-o>
inoremap [<Enter> []<Left><CR><ESC><S-o>
inoremap (<Enter> ()<Left><CR><ESC><S-o>



