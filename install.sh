#Install Git
sudo yum -y install git

#Package Install required for Ruby Install
sudo yum -y install gcc-c++ glibc-headers openssl-devel readline libyaml-devel readline-devel zlib zlib-devel libffi-devel libxml2 libxslt libxml2-devel libxslt-devel sqlite-devel

#Create Ruby Group
sudo groupadd ruby
sudo usermod -G wheel,ruby admin
sudo groups admin


#Install rbenv
cd /usr/local
git clone git://github.com/sstephenson/rbenv.git rbenv
mkdir rbenv/shims rbenv/versions rbenv/plugins

cd rbenv/plugins
git clone git://github.com/sstephenson/ruby-build.git ruby-build
cd ruby-build
./install.sh

cd /usr/local

chgrp -R ruby rbenv
chmod -R g+rwxX rbenv

echo 'export RBENV_ROOT="/usr/local/rbenv"'     >> /etc/profile.d/rbenv.sh
echo 'export PATH="/usr/local/rbenv/bin:$PATH"' >> /etc/profile.d/rbenv.sh
echo 'eval "$(rbenv init -)"'                   >> /etc/profile.d/rbenv.sh

source /etc/profile.d/rbenv.sh
echo $PATH

#Install ruby
rbenv install -v 2.3.1
rbenv global 2.3.1
rbenv rehash
rbenv versions
ruby -v

#gem update
gem update

#Install zsh
sudo yum install zsh
git clone git://github.com/robbyrussell/oh-my-zsh.git ~/.oh-my-zsh
cp ~/.oh-my-zsh/templates/zshrc.zsh-template ~/.zshrc
chsh -s /bin/zsh